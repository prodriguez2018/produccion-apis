"""
reference https://www.analyticsvidhya.com/blog/2017/09/machine-learning-models-as-apis-using-flask
"""
import os
from sentiment import api as sentiment_api
from gender import api as gender_api
from location import api as location_api
from user_twitter import api as social_api
from age import api as age_api
from flask import Flask, jsonify, request, render_template
from concepts import api as concepts_api
from flask_cors import CORS

# initialize API's
sentiment_api.initialize()
gender_api.initialize()
location_api.initialize()
social_api.initialize()
age_api.initialize()
# #concepts_api.initialize()

app = Flask(__name__)

CORS(app)

global atenea
atenea = False


def validar_atenea():
    pass


@app.route('/gender', methods=['POST'])
def gender():
    try:
        name = request.get_json(silent=True)['name']
        responses = jsonify(gender=gender_api.detect_gender(name))
        responses.status_code = 200

    except Exception as inst:
        print("Oops!  That was no valid number.  Try again...")
        responses = jsonify(gender=0, text=inst)
        responses.status_code = 500

    return responses


@app.route('/sentiment', methods=['POST'])
def sentiment():
    try:
        # text = request.args.get('text', default='', type=str)
        text = request.get_json(silent=True)['text']
        responses = jsonify(sent=sentiment_api.api(text))
        responses.status_code = 200

    except Exception as inst:
        print("Oops!  That was no valid number.  Try again...")
        responses = jsonify(sent=8, text=inst)
        responses.status_code = 500

    return responses


@app.route('/location', methods=['POST'])
def location():
    # location_api.updateCollection()

    try:
        location = request.get_json(silent=True)['loc']
        responses = jsonify(loc=location_api.detect_location(location))
        responses.status_code = 200

    except Exception as inst:
        print("Oops!  That was no valid location.  Try again...")
        responses = jsonify(location=None, text=inst)
        responses.status_code = 500

    return responses


@app.route('/location/coord', methods=['POST'])
def location_client():
    try:
        ciudad = request.get_json(silent=True)['c']
        pais = request.get_json(silent=True)['p']
        sector = request.get_json(silent=True)['s']
        responses = jsonify(loc=location_api.detect_location_client(ciudad, pais, sector))
        responses.status_code = 200

    except Exception as inst:
        print("Oops!  That was no valid location.  Try again...")
        responses = jsonify(loc=None)
        responses.status_code = 500

    return responses


@app.route('/')
def root():
    dir = os.path.dirname(__file__)
    file = os.path.join(dir, 'index.html')
    return render_template('index.html')


@app.route('/atenea', methods=['POST'])
def disable():
    global atenea
    params = request.get_json()
    var = params.get('atenea')
    atenea = var
    responses = jsonify(resp=atenea)
    responses.status_code = 200
    return responses


@app.route('/enrich', methods=['POST'])
def enrichment():
    global atenea
    if atenea:
        1 / 0
    # location_api.updateCollection()
    try:
        params = request.get_json()
        name = params.get('name')
        text = params.get('text')
        des = params.get('d')
        genero = gender_api.detect_gender(name)
        sentimiento = sentiment_api.api(text)
        # key_concepts = concepts_api.tag_tweet(text)
        age = age_api.age_detect(des)
        if params.get('loc') is not None:
            loc = params.get('loc')
            ubicacion = location_api.detect_location(loc)
            # responses = jsonify(gender=genero, sent=sentimiento, loc=ubicacion, e=age, kc=key_concepts)
            responses = jsonify(gender=genero, sent=sentimiento, loc=ubicacion, e=age)
        else:
            # responses = jsonify(gender=genero, sent=sentimiento, loc=None, kc=key_concepts)
            responses = jsonify(gender=genero, sent=sentimiento, loc=None)
        responses.status_code = 200

    except Exception as inst:
        print("Oops!  That was no valid number.  Try again...")
        responses = jsonify({'gender': 0, 'sent': 8, 'location': None, 'text': inst})
        responses.status_code = 500

    return responses


@app.route('/social/username', methods=['GET'])
def user_by_screen_name():
    try:
        sc = request.args.get('sc')
        user = social_api.find_by_screenName(sc)
        responses = jsonify(user=user)
        responses.status_code = 200
    except Exception as inst:
        print("Oops!  That was no valid screen name.  Try again...")
        responses = jsonify({"_id": None, "sc": None, "nm": None})
        responses.status_code = 500

    return responses


@app.route('/social/userid', methods=['GET'])
def user_by_id():
    try:
        id = request.args.get('id')
        user = social_api.find_by_id(id)
        responses = jsonify(user)
        responses.status_code = 200
    except Exception as inst:
        print("Oops!  That was no valid screen name.  Try again...")
        responses = jsonify({"_id": None, "sc": None, "nm": None})
        responses.status_code = 500

    return responses


@app.route('/social/tw', methods=['POST'])
def social_user():
    try:
        params = request.get_json()
        msj = social_api.bg_insert(params)
        responses = jsonify({"msj": msj})
        responses.status_code = 200
    except Exception as inst:
        print("Oops!  That was no valid user.  Try again...")
        responses = jsonify({"user": None})
        responses.status_code = 500

    return responses