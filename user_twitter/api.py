import pymongo
import logging
import config_server

global db_conexion
global logger


def initialize():
    connection = pymongo.MongoClient(config_server.db_name)
    #connection = pymongo.MongoClient("mongodb://192.168.1.217")
    # connection = pymongo.MongoClient("mongodb://mongodb")
    global db_conexion
    global logger
    db_conexion = connection['social-profiles']
    # logger = logging.getLogger('apis')
    # logger.setLevel(logging.INFO)
    # logger.info('Conexion a api user_twitter creada')
    print('Conexion Apis USER_TWITTER Inicializada')


def find_by_id(id):
    if id is not None:
        query = db_conexion.twitter.find_one({"_id": id})
        if query:
            return query
        else:
            return {"user": None}
    else:
        return {"user": None}


def find_by_screenName(sc):
    if sc is not None:
        query = db_conexion.twitter.find_one({"sc": sc})
        if query:
            return query
        else:
            return {"user": None}
    else:
        return {"user": None}


def bg_insert(lista):
    for i in lista:
        try:
            temp = db_conexion.twitter.find_one({"_id": i['_id']})
            if temp:
                hist_sc = set(temp['scl'])
                hist_nm = set(temp['nml'])
                hist_sc.add(i['sc'])
                hist_nm.add(i['nm'])
                hist_sc = list(hist_sc)
                hist_nm = list(hist_nm)
                user = {'_id': str(i['_id']), 'sc': i['sc'], 'nm': i['nm'], 'nml': hist_nm, 'scl': hist_sc}
                db_conexion.twitter.save(user)
            else:
                hist_sc = []
                hist_nm = []
                hist_sc.append(i['sc'])
                hist_nm.append(i['nm'])
                user = {'_id': str(i['_id']), 'sc': i['sc'], 'nm': i['nm'], 'scl': hist_sc, 'nml': hist_nm}
                db_conexion.twitter.save(user)
        except Exception as inst:
            global logger
            # logger.info("Error en insertar:" + i)
            return "error"

    return "ok"
