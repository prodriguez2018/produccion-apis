from nltk.classify import NaiveBayesClassifier

from sentiment.text_cleaning import quitarTildes, limpiezaTexto, stem, format_sentence
import pymongo
import logging
import os
import config_server
from nltk.tokenize import word_tokenize

db_conexion=''
model=''
tweets=[]
stopwords=[]

logger = logging.getLogger('apis')
logger.setLevel(logging.DEBUG)
logger.info("Init")


def initialize():
   connection = pymongo.MongoClient(config_server.db_name)
   # connection = pymongo.MongoClient("mongodb://mongodb")
   global db_conexion
   # global stopwords
   db_conexion = connection['apisdb']
   logger.info('Conexion a apis creada')
   load_tweets()
   load_stopwords()
   trainModel()
   print("Conexion Apis SENTIMIENTO Inicializada")


def load_tweets():
   global tweets
   global db_conexion
   print("Cargando tweets")
   temp_pos = db_conexion.tweets.find({}, {"_id": 0, "texto": 1, "sent": 1});
   for i in temp_pos:
       temp = clean_text(i['texto'])
       tweets.append([format_sentence(temp), i['sent']])


def load_stopwords():
   global stopwords
   q_sw = db_conexion.stopwords.find({}, {"_id": 0, "sw": 1})
   stopwords = [s['sw'] for s in q_sw]
   #print(stopwords)


def trainModel():
   global model
   model = NaiveBayesClassifier.train(tweets)  # Entrena el modelo


def clean_text(tweet):
   cadenaLimpia = quitarTildes(tweet)
   cadenaLimpia = limpiezaTexto(cadenaLimpia)
   cadenaLimpia = quitar_stop_words(cadenaLimpia)
   cadenaLimpia = stem(cadenaLimpia)
   #print(cadenaLimpia)
   return cadenaLimpia


# Remueve stopwords
def quitar_stop_words(tweet):
   stopwords
   word_tokens = word_tokenize(tweet)
   filtered_sentence_words = []
   for w in word_tokens:
       if w not in stopwords:
           filtered_sentence_words.append(w)

   delimiter = ' '
   # Concatena los elementos del array
   sentence = delimiter.join(filtered_sentence_words)
   return (sentence)


'''
   Sentimiento:   1.- Positivo
                  0.- Neutro
                 -1.- Negativo
                  8.- Ambiguo
'''


def api(tweet):
   temp = clean_text(tweet)
   if len(temp) > 0:
       # print(clean_text(temp))
       format_text = format_sentence(clean_text(temp))
       return model.classify(format_text)
   else:
       return '8'

# initialize()
# load_tweets()
# load_stopwords()
# trainModel()

#     Test analisis de sentimientos
# '''
# while True:
#     stringAnalyze = input("Ingrese la frase a analizar: ")
#     text_norm = clean_text(stringAnalyze)
#     if len(text_norm) > 0:
#         format_text = format_sentence(text_norm)
#         print(format_text)
#         sent = model.classify(format_text)
#         print(sent)