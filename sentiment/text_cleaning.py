import re
import unicodedata
import nltk
import os
# import logging

from nltk import SnowballStemmer

### ---API Sentimientos (Servicios)---  ###
# def initialize():

   # logger = logging.getLogger('apis')
   # logger.setLevel(logging.INFO)
   # logger.info('Conexion a stopwords creada')

# Inicializa el extractor de raices lexicales
def stem(word):
   stemmer = SnowballStemmer('spanish')
   return stemmer.stem(word)


# Remueve agentes externos
def limpiezaTexto(cadena):
   sLinks = ' '.join(re.sub("(:[a-zA-z]+:_?)+|(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|([0-9]+0)|(\w+:\/\/\S+)|[0-9]+", " ",
                            cadena).split())  # Elimina links, retweets, users..
   sBlancos = re.sub('[ \t]{2,}', '', sLinks)  # Eliminar blancos
   sBlancos = re.sub('^\\s+|\\s+$', '', sBlancos)  # Eliminar blancos
   return sBlancos


# Convierte la frase a minuscula y remueve tildes
def quitarTildes(cadena):
   cadena = cadena.lower()
   clean = ''.join(
       (c for c in unicodedata.normalize('NFD', cadena) if unicodedata.category(c) != 'Mn'))  # Quitar tildes
   return (clean)


# Convierte la frase en tokens
def format_sentence(sent):
   return {word: True for word in nltk.word_tokenize(sent)}