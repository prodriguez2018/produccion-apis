import os

import dill as pickle
from gender.train import extract_feature
from sentiment.text_cleaning import quitarTildes

filename = 'model_v1.pk'
global model


def initialize():
    global model
    dir = os.path.dirname(__file__)
    file = os.path.join(dir, 'files/' + filename)
    with open(file, 'rb') as f:
        model = pickle.load(f)


def detect_gender(name):
    if len(name) > 0:
        splitted = name.split()
        firstName = quitarTildes(splitted[0])
        return model.classify(extract_feature(firstName))  # Clasifica texto ingresado
    else:
        return -1
