import os
import random
import unicodedata
from zipfile import ZipFile
from nltk import NaiveBayesClassifier, classify
import dill as pickle

# import logging

# logger = logging.getLogger('apis')
# logger.setLevel(logging.INFO)

filename = 'model_v1.pk'
gender_map = {'M': 0, 'F': 1}


def load_names(zip_file='./files/names.zip'):
    """
    Names  = {Unique_Name[Male_frequency_count, Female_frequency_count]}
    :param zip_file: names.zip Dataset
    :return: dict() con los nombres y la frecuencia
    """
    if not os.path.isfile(zip_file):
        # logger.info('names.zip is missing.')
        print('names.zip is missing.')
        exit(-1)

    names = dict()
    unzip = ZipFile(zip_file, 'r')
    files = unzip.namelist()

    for file in files:
        file = unzip.open(file, 'r').read().decode('utf-8')
        rows = [row.strip().split(',') for row in file.split('\n') if len(row) > 1]
        for row in rows:
            if not len(row) == 3:
                continue
            name = row[0].upper()
            gender = gender_map[row[1].upper()]
            count = int(row[2])
            # anandimos la frecuencia por genero
            if name not in names:
                names[name] = [0, 0]
            names[name][gender] += count
    return names


def split_names(names: dict()):
    """
    # convertimos la lista en una tupla de nombres (name, male_freq_count, female_freq_count)
    # ya q un nombre puede ser tanto para hombres o mujeres, ponemos el nombre y la frecuencia por genero
    :param names: dict() containing names and frequency count
    :return: names tuple (male_names, female_names.txt)
    """
    if not names:
        # logging.info('names dict is none.')
        exit(-1)

    male_names = list()
    female_names = list()

    for name in names.keys():
        counts = names[name]
        male_counts, female_counts = counts[0], counts[1]
        data = (name, male_counts, female_counts)
        if male_counts == female_counts:
            continue
        if male_counts > female_counts:
            male_names.append(data)
        else:
            female_names.append(data)

    names = (male_names, female_names)
    total_males_names = len(male_names)
    total_females_names = len(female_names)
    total_names = total_females_names + total_males_names
    # logger.info('Total nombres - {} \n Total hombres - '
    #              '{} \n Total mujeres - {}'.format(total_names, total_males_names,
    #                                                total_females_names))
    return names


def extract_feature(name: str):
    """
    Atributos para la caracterización de los datos
    :param name: string
    :return: dict of feature values
    """
    name = name.upper()
    #QUITAR TILDES
    clean = ''.join(
        (c for c in unicodedata.normalize('NFD', name) if unicodedata.category(c) != 'Mn'))
    return {
        'last_1': clean[-1],
        'last_2': clean[-2:],
        'last_3': clean[-3:],
        'last_is_vowel': (clean[-1] in 'AEIOU')
    }


def get_probability_distribution(name_tuple):
    """
    Applying probability distribution as One name have two outcomes.
    male_probability = total_male_count / (total_male_count + total_female_count).
    No system can hold the value 1.0 in probability so making it to 0.99.
    :param name_tuple: name tuple contains male / female frequency count
    :return: male, female probability
    """
    male_counts = name_tuple[1]
    female_counts = name_tuple[2]
    male_prob = (male_counts * 1.0) / sum([male_counts, female_counts])
    if male_prob == 1.0:
        male_prob = 0.99
    elif male_prob == 0.00:
        male_prob = 0.01
    female_prob = 1.0 - male_prob
    return male_prob, female_prob


def prepare_data_set():
    """
    Preparamos la matriz de caracteristicas (X) y el vector de respuesta (y) - Supervised Learning model.
    :param names: tuple con los nombres de hombres y mujeres
    :return:
    """
    feature_set = list()
    male_names, female_names = split_names(load_names())
    names = {'M': male_names, 'F': female_names}

    for gender in names.keys():
        for name in names[gender]:
            features = extract_feature(name[0])
            male_prob, female_prob = get_probability_distribution(name)
            features['m_prob'] = male_prob
            features['f_prob'] = female_prob
            feature_set.append((features, gender))
    random.shuffle(feature_set)
    return feature_set


def validate_data_set(feature_set: list):
    """
    :param feature_set: validation purpose
    :return: None
    """
    data_list = []
    for feature_value, gender in feature_set:
        data_list.append({**feature_value, **{'gender': gender}})

    import pandas as pd
    df = pd.DataFrame(data_list)
    # logger.info('matriz de features- ', df.shape)


def train_and_test(train_percent=None):
    """
    divide dataset y encuentra el accuracy.
    :param train_percent: split ratio
    :return:
    """
    feature_set = prepare_data_set()
    validate_data_set(feature_set)
    random.shuffle(feature_set)
    total = len(feature_set)
    # cut_point = int(total * train_percent)
    # dividimos en datos de entrenar y test
    # train_set = feature_set[:cut_point]
    # test_set = feature_set[cut_point:]

    # entrnemaos y clasificamos el dataset
    classifier = NaiveBayesClassifier.train(feature_set)
    # logger.info('{} Accuracy- {}'.format('Naive Bayes', classify.accuracy(classifier, feature_set)))
    # logger.info('Most informative features')
    informative_features = classifier.most_informative_features(n=5)
    for feature in informative_features:
        print("\t {} = {} ".format(*feature))
    ''' persistir modelo
    '''
    with open('./files/' + filename, 'wb') as file:
        pickle.dump(classifier, file)
    return classifier


def model_evaluation(classifier):
    """
    Manual testing of the model.
    :param classifier: object to test run time names
    :return:
    """
    # logger.info('<<<  Test modelo >>> \n')
    # logger.info('"q" para salir \n')
    while 1:
        test_name = input('Ingrese nombre para clasificar: ')
        if test_name.lower() == 'q' or test_name.lower() == 'quit':
            # logger.info('End')
            print('End')
            exit(1)
        if not test_name:
            continue

        result = classifier.classify(extract_feature(test_name))
        if gender_map[result.upper()]:
            result = 'Mujer'
        else:
            result = 'Hombre'
            # logger.info('{} es {}'.format(test_name, result))

            # model_evaluation(train_and_test())

#clasif=train_and_test()
#model_evaluation(clasif)
