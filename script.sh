#!/bin/bash

mkdir -p enrichment-apis/{age,gender,location,sentiment,user_twitter,concepts}

cp -r templates enrichment-apis

cp -r gender/files enrichment-apis/gender

cp -r sentiment/files enrichment-apis/sentiment

for FILE in __pycache__/*.pyc ; do NEWFILE=`echo $FILE | sed 's/cpython-36.//g'` ; mv "$FILE" $NEWFILE ; done
for FILE in __pycache__/*.pyc ; do mv $FILE enrichment-apis ; done

for FILE in age/__pycache__/*.pyc ; do NEWFILE=`echo $FILE | sed 's/cpython-36.//g'` ; mv "$FILE" $NEWFILE ; done
for FILE in age/__pycache__/*.pyc ; do mv $FILE enrichment-apis/age ; done

for FILE in gender/__pycache__/*.pyc ; do NEWFILE=`echo $FILE | sed 's/cpython-36.//g'` ; mv "$FILE" $NEWFILE ; done
for FILE in gender/__pycache__/*.pyc ; do mv $FILE enrichment-apis/gender ; done

for FILE in location/__pycache__/*.pyc ; do NEWFILE=`echo $FILE | sed 's/cpython-36.//g'` ; mv "$FILE" $NEWFILE ; done
for FILE in location/__pycache__/*.pyc ; do mv $FILE enrichment-apis/location ; done

for FILE in sentiment/__pycache__/*.pyc ; do NEWFILE=`echo $FILE | sed 's/cpython-36.//g'` ; mv "$FILE" $NEWFILE ; done
for FILE in sentiment/__pycache__/*.pyc ; do mv $FILE enrichment-apis/sentiment ; done

for FILE in user_twitter/__pycache__/*.pyc ; do NEWFILE=`echo $FILE | sed 's/cpython-36.//g'` ; mv "$FILE" $NEWFILE ; done
for FILE in user_twitter/__pycache__/*.pyc ; do mv $FILE enrichment-apis/user_twitter ; done

for FILE in concepts/__pycache__/*.pyc ; do NEWFILE=`echo $FILE | sed 's/cpython-36.//g'` ; mv "$FILE" $NEWFILE ; done
for FILE in concepts/__pycache__/*.pyc ; do mv $FILE enrichment-apis/concepts ; done