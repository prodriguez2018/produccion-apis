import pymongo


def etl_tweets():
    print("Cargando tweets....")
    connection = pymongo.MongoClient("mongodb://192.168.1.217")
    db_conexion_ciudades = connection['apisdb']

    location = []
    #Ingreso de texto positivo a la base
    with open("pos.txt", encoding="utf8") as f:
        for i in f:
            location = i.split('\n')
            a = location[0].strip()
            a = a.strip("\n")
            ciudad = {"texto": a,
                      "sent": 1,
                      }

            db_conexion_ciudades.tweets.insert_one(ciudad)


def stopwords():
    print("Cargando ciudades....")
    connection = pymongo.MongoClient("mongodb://192.168.1.217")
    db_conexion_ciudades = connection['apisdb']

    location = []
    with open("stopwords.csv", encoding="utf8") as f:
        for i in f:
            location = i.split('\n')
            a = location[0].strip()
            a = a.strip(";")
            ciudad = {"sw": a}

            db_conexion_ciudades.stopwords.insert_one(ciudad)

#stopwords()
#etl_cities()
