import pymongo
import unicodedata


def etl_cities():
    print("Cargando ciudades....")
    connection = pymongo.MongoClient("mongodb://192.168.1.217")
    db_conexion_ciudades = connection['socialLocation']

    location = []
    with open("EC.txt", encoding="utf8") as f:
        for i in f:
            location = i.split('\t')
            ciudad = {"geonameid": location[0],
                      "name": location[1],
                      "asciiname": location[2],
                      "alternatenames": location[3],
                      "latitude": location[4],
                      "longitude": location[5],
                      "feature_class": location[6],
                      "feature_code": location[7],
                      "country_code": location[8],
                      "cc2": location[9],
                      "admin1_code": location[10],
                      "admin2_code": location[11],
                      "admin3_code": location[12],
                      "admin4_code ": location[13],
                      "population": location[14],
                      "elevation": location[15],
                      "dem": location[16],
                      "timezone": location[17],
                      }

            db_conexion_ciudades.ciudadPais.insert_one(ciudad)


def etl_citiesAdmin():
    print("Cargando ciudades admin....")
    connection = pymongo.MongoClient("mongodb://192.168.1.217")
    db_conexion_ciudades = connection['socialLocation']

    with open("admin2Codes.txt", encoding="utf8") as f:
        for i in f:
            location = i.split('\t')
            if "ECU" in location[0] or "DOM" in location[0]:
                ciudad = {"code": location[0],
                          "name": location[1],
                          "asciiname": location[2],
                          "geonameId": location[3],
                          }

                db_conexion_ciudades.ciudadesAdmin.insert_one(ciudad)


def final_etl():
    print("match ciudades vs admin....")
    connection = pymongo.MongoClient("mongodb://192.168.1.217")
    db_conexion_ciudades = connection['socialLocation']
    query = db_conexion_ciudades.ciudadPais.find({"feature_class": "A", "feature_class": "P"})

    for i in query:
        a = i["country_code"]
        b = i["admin1_code"]
        c = i["admin2_code"]

        code = a + "." + b + "." + c
        query2 = db_conexion_ciudades.ciudadesAdmin.find_one({"code": code})

        if query2:
            d = query2["name"].lower()
        else:
            d = None

        if len(i["alternatenames"]) > 0:
            c_a = i["alternatenames"].split(",")
            for j in c_a:
                ciudad_f = {"x": elimina_tildes(j.lower()),
                            "p": i["country_code"].upper(),
                            "c": d,
                            "s": i["name"].lower(),
                            "lat": i["latitude"],
                            "lon": i["longitude"],
                            }
                db_conexion_ciudades.geoNamesDb.insert_one(ciudad_f)
        else:
            ciudad_f = {"x": elimina_tildes(i["name"].lower()),
                        "p": i["country_code"].upper(),
                        "c": d,
                        "s": i["name"].lower(),
                        "lat": i["latitude"],
                        "lon": i["longitude"],
                        }
            db_conexion_ciudades.geoNamesDb.insert_one(ciudad_f)


def elimina_tildes(s):
    return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))

# etl_cities()
# etl_citiesAdmin()
#final_etl()
