import re
import datetime
import emoji

global RE_EMOJI
global RE_YEAR_F_1
global RE_YEAR_F_2
global RE_YEAR_CALC
global RE_JOB_CALC

global inter_1
global inter_2
global inter_3
global inter_4
global inter_5
global inter_u
global range_1
global range_2
global range_3
global range_4
global range_5


def initialize():
    global RE_EMOJI
    global RE_YEAR_F_1
    global RE_YEAR_F_2
    global RE_YEAR_CALC
    global RE_JOB_CALC
    global RE_JOB_SUP

    global inter_1
    global inter_2
    global inter_3
    global inter_4
    global inter_5
    global inter_u

    global range_1
    global range_2
    global range_3
    global range_4
    global range_5

    RE_YEAR_F_1 = r"(^|\b)([0-9]{1,2}) (años|anios|years? old)"#Primer criterio de busqueda
    RE_YEAR_F_2 = r"(estudiante|estudio) (en|de)*"#Tercer criterio de busqueda
    RE_YEAR_CALC = r"((en|desde) *(19[3-9]{1}[2-9]{1})|(20[0-9]{2}) *)"#Segundo criterio de busqueda
    RE_JOB_CALC = r"(trabaj(a|o|e|é)|profesional|expert(o|a))"#Cuarto criterio de busqueda
    RE_JOB_SUP = r"(gerente|president(a|e))"#Quinto criterio de busqueda

    inter_1 = "0"  # -17
    inter_2 = "1"  # 18-25
    inter_3 = "2"  # 26-40
    inter_4 = "3"  # 41-55
    inter_5 = "4"  # 55+
    inter_u = "8"  # indefinido

    range_1 = range(1, 18)
    range_2 = range(18, 26)
    range_3 = range(26, 41)
    range_4 = range(41, 56)
    range_5 = range(56, 100)
    print('Conexion Apis EDAD Inicializada')


def age_detect(text):
    des = str(text)
    des = des.lower()
    try:
        list_year_f = re.search(RE_YEAR_F_1, des, re.MULTILINE).groups()
        if len(list_year_f) > 0:
            num_list = []
            for i in list_year_f:
                if i is not None and i.isnumeric():
                    num_list.append(int(i))
            if len(num_list) > 0:
                max_num = max(num_list)
                range_age = get_range(max_num)
                return range_age

    except Exception as e:
        pass
        # print("RE_YEAR_F_1 not found")

    try:
        list_year_calc = re.search(RE_YEAR_CALC, des, re.MULTILINE).groups()
        if len(list_year_calc) > 0:
            num_list = []
            for i in list_year_calc:
                if i is not None and i.isnumeric():
                    num_list.append(i)
            if len(num_list) > 0:
                min_num = int(min(num_list))
                now = datetime.datetime.now()
                edad = now.year - min_num
                range_age = get_range(edad)
                return range_age
    except Exception as e:
        pass
        # print("RE_YEAR_CALC not found")

    try:
        list_des_year = re.search(RE_YEAR_F_2, des, re.MULTILINE).groups()
        if len(list_des_year):
            return inter_2
    except Exception as e:
        pass
        # print("RE_YEAR_F_2 not found")

    try:
        list_des_jobs = re.search(RE_JOB_CALC, des, re.MULTILINE).groups()
        if len(list_des_jobs):
            return inter_3
    except Exception as e:
        pass
        # print("RE_JOB_CALC not found")

    try:
        list_des_jobs = re.search(RE_JOB_SUP, des, re.MULTILINE).groups()
        if len(list_des_jobs):
            return inter_4
    except Exception as e:
        pass
        # print("RE_JOB_SUP not found")

    try:
        list_emoji = extract_emoji(des)
        if len(list_emoji) > 0:
            return inter_2
    except Exception as e:
        pass
        # print("RE_EMOJI not found")

    return inter_u


def extract_emoji(str):
    return ''.join(c for c in str if c in emoji.UNICODE_EMOJI)


def get_range(num):
    if num in range_1:
        return inter_1
    elif num in range_2:
        return inter_2
    elif num in range_3:
        return inter_3
    elif num in range_4:
        return inter_4
    elif num in range_5:
        return inter_5
    else:
        return inter_u
