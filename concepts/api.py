import textacy
import spacy
import re
# import logging

global url
global simbol
global user
global nlp
global stop_conceps
# global logger


def initialize():
    global url
    global simbol
    global user
    global nlp
    global stop_concepts
    # global logger

    # logger = logging.getLogger('apis')
    # logger.setLevel(logging.INFO)
    # logger.info("Initialize spacy")
    nlp = spacy.load('es')
    # logger.info("Final spacy")
    stop_concepts = ["años", "vía", "q"]
    url = re.compile(r'(\w+:\/\/\S+)', flags=re.MULTILINE)  # remove links
    simbol = re.compile(r'“|”|»|´|>|►|<|\||(\[|\()#?[a-zA-Z]*(\]|\))', flags=re.MULTILINE)  # remove simbols
    user = re.compile(r'^(((@[A-Za-z0-9]+)|([^0-9A-Za-z \t])) ?)+|(((@[A-Za-z0-9]+)|([^0-9A-Za-z \t])) ?)+$',
                      flags=re.MULTILINE)  # remove username


def depureTweet(tweet):
    ch_text = url.sub('', tweet)
    ch_text = simbol.sub('', ch_text)
    ch_text = user.sub('', ch_text)
    ch_text = str(ch_text).lstrip()
    ch_text = str(ch_text).rstrip()
    return ch_text


def tag_tweet(tweet):
    # logging.info("Init concepts")
    if len(tweet) < 280:
        ch_text = depureTweet(tweet)
        doc = nlp(ch_text)
        ngrams = textacy.extract.ngrams(doc, n=2, filter_stops=True, filter_punct=False)
        key_concepts = []
        for i in ngrams:

            # case 1
            if i[0].pos_ == 'PROPN' and i[1].pos_ == 'ADJ':
                key_concepts.append(str(i[0]).title() + ' ' + str(i[1]).lower())
                continue

            # case 2
            elif i[0].pos_ == 'NUM' and i[1].pos_ == 'NOUN':
                key_concepts.append(str(i[0]) + ' ' + str(i[1]).lower())
                continue

            # case 3
            elif i[0].pos_ == 'NOUN' and i[1].pos_ == 'ADJ':
                key_concepts.append(str(i[0]).lower() + ' ' + str(i[1]).lower())
                continue

            # case 4
            elif i[0].pos_ == 'ADJ':
                key_concepts.append(str(i[0]).lower())
                continue

            # case 5
            elif i[0].pos_ == 'NOUN':
                key_concepts.append(str(i[0]).lower())
                continue

            # case 1
            elif i[0].pos_ == 'PROPN':
                if i[1].pos_ == 'PROPN':
                    key_concepts.append(str(i[0]).title() + ' ' + str(i[1]).title())
                else:
                    key_concepts.append(str(i[0]).title())
                continue

            # case 6
            elif i[1].pos_ == 'PROPN':
                key_concepts.append(str(i[1]).title())

        # remueve palabras repetidas
        key_temp = set(key_concepts)
        key_final = set()
        for j in key_temp:
            if len(j.split()) == 1:
                aux = list(key_temp)
                aux.remove(j)
                if len(j) > 3 and j not in str(aux) and j not in stop_concepts:
                    key_final.add(j)
            else:
                key_final.add(j)

        return list(key_final)

    else:
        return list()
