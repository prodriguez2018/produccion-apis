import pymongo
import re
import logging
import config_server
from location.etl_location import limpiezaTexto

global db_conexion_ciudades
global logger


### ---API Ubicaciones---  ###
def initialize():
    #connection = pymongo.MongoClient("mongodb://192.168.1.217")
    connection = pymongo.MongoClient(config_server.db_name)
    # connection = pymongo.MongoClient("mongodb://mongodb")
    global db_conexion_ciudades
    #logger.setLevel(logging.INFO)
    db_conexion_ciudades = connection['socialLocation']
    # logger.info('Conexion api ubicaciones')
    print('Conexion Apis UBICACIONES Inicializada')


def detect_location(local):
    if local is not None:

        splitted = re.split(",|-", local)
        if len(splitted) == 1:
            # Tiene solo ciudad
            query = db_conexion_ciudades.geoNamesDb.find({"x": limpiezaTexto(splitted[0])}).sort(
                [("pref", pymongo.ASCENDING)])
            if query.count() >= 1:
                return {"s": query[0].get('s', None), "c": query[0]['c'], "p": query[0]['p']}
            else:
                return {}
        elif len(splitted) > 1:
            # Tiene ciudad y pais
            c1 = splitted[0].strip()
            c2 = splitted[1].strip()
            query1 = db_conexion_ciudades.geoNamesDb.find({"x": limpiezaTexto(c1)}, {"p": 1, "c": 1, }).sort(
                [("pref", pymongo.ASCENDING)])

            # Si no hay resultados por ciudad se busca por país
            if query1.count() == 0:
                query2 = db_conexion_ciudades.geoNamesDb.find({"x": limpiezaTexto(c2)}).sort(
                    [("pref", pymongo.ASCENDING)]).limit(1)
                if query2.count() > 1:
                    return {"s": query2[0].get('s', None), "c": query2[0]['c'], "p": query2[0]['p']}
                else:
                    return {}

            # Si hay un único resultado por el parámetro 1 se devuelve
            elif query1.count() == 1:
                return {"s": query1[0].get('s', None), "c": query1[0]['c'], "p": query1[0]['p']}

            # Si hay mas de un resultado se hace match entre ciudad y país para buscar coincidencias
            elif query1.count() > 1:
                query2 = db_conexion_ciudades.geoNamesDb.find({"x": limpiezaTexto(c2)}, {"p": 1, "c": 1, })
                if query2.count() > 0:
                    query1List = list(query1)
                    for subq2 in query2:
                        pais = subq2['p']
                        for subq1 in query1List:
                            if subq1['p'] == pais:
                                return {"s": subq1.get('s', None), "c": subq1['c'], "p": subq1['p']}
                    return {"s": query1List[0].get('s', None), "c": query1List[0]['c'], "p": query1List[0]['p']}
                else:
                    return {"s": query1[0].get('s', None), "c": query1[0]['c'], "p": query1[0]['p']}

        # Si no hay resultados se devuelve vacío
        elif len(splitted) == 0:
            return {"loc": ""}
    else:
        return {"loc": ""}





def detect_location_client(c, p, s):
    if s is not None and len(s) > 0:
        query = db_conexion_ciudades.geoNamesDb.find_one(
            {"p": p, "c": c, "s": s})
    else:
        query = db_conexion_ciudades.geoNamesDb.find_one(
            {"p": p, "c": c})

    if query:
        ubicacion = {"lat": query['lat'], "lon": query['lon']}
    else:
        ubicacion = {}
    return ubicacion


