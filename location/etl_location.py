import unicodedata

# This code doesn't work
# import pymongo

# global db_conexion_ciudades


### ---Ubicaciones (Servicios)---  ###
# def initialize():
#     connection = pymongo.MongoClient("mongodb://192.168.1.212")
#     # connection = pymongo.MongoClient("mongodb://mongodb")
#     global db_conexion_ciudades
#     db_conexion_ciudades = connection['socialLocation']
#     print('Conexion servicios ubicaciones')


####       ETL DB        ####
# def updateCiudad():
#     connection = pymongo.MongoClient("mongodb://192.168.1.212")
#     db_conexion_ciudades = connection['locationdb']
#     result = db_conexion_ciudades.geoNamesDb.find()
#     for doc in result:
#         if doc['c'] is not None and "canton" in doc['c']:
#             q_canton = doc['c'].replace("canton ", "")
#             db_conexion_ciudades.geoNamesDb.update({"_id": doc['_id']},
#                                                    {'$set': {'c': q_canton}})
#             print(doc['x'])
#             print(result)


# Elimina espacios en blanco, minúsculas y sin tildes
def limpiezaTexto(cadena):
    result = ''.join((c for c in unicodedata.normalize('NFD', cadena) if unicodedata.category(c) != 'Mn'))
    result = result.lower()
    result = result.strip()
    return result

# Elimina tildes de las ciudades, paises
# def elimina_tildes(s):
#     return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))


# Se normaliza los datos sin tildes, minúsculas
# def updateCollection():
#     result = db_conexion_ciudades.geoNamesDb.find()
#     for doc in result:
#         result = elimina_tildes(doc['x'].lower())
#         db_conexion_ciudades.geoNamesDb.update({"_id": doc['_id']}, {'$set': {'x': elimina_tildes(doc['x'].lower())}})
#         print(doc['x'])
#         print(result)


# Se normaliza los datos sin tildes, minúsculas y sólo país a mayusculas
# def migrate_location():
#     connection1 = pymongo.MongoClient("mongodb://192.168.1.212")
#     connection2 = pymongo.MongoClient("mongodb://192.168.1.212")
#     db_conexion_ciudades = connection1['ciudadesdb']
#     db_conexion_location = connection2['socialLocation']
#
#     des = db_conexion_ciudades.ciudadPais.find({}, {"_id": 0, "_class": 0, })
#     for i in des:
#         query = db_conexion_location.geoNamesDb.find_one(
#             {"x": elimina_tildes(i["x"].lower()),
#              "c": elimina_tildes(i["c"].lower()),
#              "p": elimina_tildes(i["p"].lower())
#              })
#         if not query:
#             obj = {
#                 "x": elimina_tildes(i["x"].lower()),
#                 "p": i["p"].upper(),
#                 "c": i["c"].lower()
#             }
#             print(str(obj))
#             db_conexion_location.geoNamesDb.insert_one(obj)


# print("Inicio")
# initialize()
# updateCollection()
# print("Fin")
# updateCiudad()
# migrate_location()
# initialize()
